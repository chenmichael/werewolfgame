# Werewolf Online

Real time online multiplayer werewolf game based on SignalR and ASP.NET Core.

## Communication

```mermaid
sequenceDiagram
    participant C as Host
    participant S as Server
    participant E as Everyone
    participant W as Werewolfes
    participant V as Seer
    C->>+S: ChangeUsername
    S-->>-C: Maybe<Username>
    C->>+S: CreateLobby
    S-->>-C: Maybe<LobbyId>
    E->>+S: JoinLobby
    S-->>-E: Maybe<LobbyId>
    C->>+S: StartGame
    S-->>-C: Maybe
    S->>+E: GameStarted
    S->>S: PickPlayerRoles
    S->>E: RoleAssigned
    loop is game over
        S->>E: Night(Updates)
        S->>E: Message(Werewolfes)
        loop while choice ambiguous
            loop while bad selection
	            S->>+W: Awake(Werewolfes)
	            W->>+S: SelectPlayer
	        end
            S->>-W: VoteReceived
            S->>W: Sleep
            deactivate W
        end
        S->>E: Message(Seer)
        alt Seer alive
            loop while bad selection
	            S->>+V: Awake(Seer)
	            V->>+S: SelectPlayer
	        end
            S->>V: VoteReceived
            S->>-V: PlayerInfo
            S->>V: Sleep
            deactivate V
        end
        S->>E: Day(updates)
        S->>+E: Awake(Everyone)
        E->>-S: SelectPlayer
		S->>E: Sleep
    end
    S->>-E: GameOver
    E->>+S: LeaveLobby
    S-->>-E: Maybe
```