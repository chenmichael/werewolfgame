using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.SignalR;
using Microsoft.Extensions.Hosting;
using System;
using WerewolfGame.Hubs;
using WerewolfGame.Hubs.Interfaces;

namespace WerewolfGame {
    /// <summary>
    /// Provides the startup class for the application.
    /// For the main entry point see <see cref="Main(string[])"/>.
    /// </summary>
    public class Program {
        private static IHubContext<GameHub, IGameClient>? hubContext;
        internal static IHubContext<GameHub, IGameClient> HubContext { get => hubContext ?? throw new InvalidProgramException("Hub context not (yet) aquired!"); set => hubContext = value; }

        /// <summary>
        /// Creates and runs ASP.NET MVC application with SignalR and a Werewolf Game Hub <see cref="GameHub"/>.
        /// </summary>
        public static void Main(string[] args) {
            var host = CreateHostBuilder(args).Build();
            hubContext = (IHubContext<GameHub, IGameClient>)(host.Services.GetService(typeof(IHubContext<GameHub, IGameClient>))
                ?? throw new InvalidProgramException("Service not found!"));
            host.Run();
        }

        /// <summary>
        /// Generates the Host builder.
        /// </summary>
        /// <returns>Host builder for the application.</returns>
        public static IHostBuilder CreateHostBuilder(string[] args) =>
            Host.CreateDefaultBuilder(args)
                .ConfigureWebHostDefaults(webBuilder => {
                    webBuilder.UseStartup<Startup>();
                });
    }
}
