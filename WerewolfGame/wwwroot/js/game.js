﻿"use strict";

const connection = new signalR.HubConnectionBuilder()
    .withUrl("/game")
    .build();

const UI = new (function UIControl() {
    var forceElementById = (elementId) => {
        const element = document.getElementById(elementId);
        if (element === null) throw new Error(`Element #${elementId} not found!`);
        return element;
    }

    this.btnchangeuser = forceElementById("btnchangeuser");
    this.btncreatelobby = forceElementById("btncreatelobby");
    this.btnjoinlobby = forceElementById("btnjoinlobby");
    this.btnleavelobby = forceElementById("btnleavelobby");
    this.btnstartgame = forceElementById("btnstartgame");
    this.btnvoteplayer = forceElementById("btnvoteplayer");
    this.cbxvoteoptions = forceElementById("cbxvoteoptions");
    this.cssdarktheme = forceElementById("cssdarktheme");

    this.divalert = forceElementById("divalert");
    this.diventerlobby = forceElementById("diventerlobby");
    this.divinlobby = forceElementById("divinlobby");
    this.divlogin = forceElementById("divlogin");
    this.divvote = forceElementById("divvote");
    this.divconnectioninfo = forceElementById("divconnectioninfo");
    this.divdead = forceElementById("divdead");

    this.lblconnectionstate = forceElementById("lblconnectionstate");
    this.lbllobby = forceElementById("lbllobby");
    this.lbllobbyfeedback = forceElementById("lbllobbyfeedback");
    this.lblloginfeedback = forceElementById("lblloginfeedback");
    this.tbxlobbyid = forceElementById("tbxlobbyid");
    this.tbxusername = forceElementById("tbxusername");

    this.uiElements = {
        all: [this.diventerlobby, this.divinlobby, this.divlogin, this.divvote, this.divconnectioninfo, this.divdead],
        // not connected
        disconnected: [],
        // connected and not logged in
        connected: [this.divlogin, this.divconnectioninfo],
        // logged in with a username
        loggedin: [this.divlogin, this.diventerlobby, this.divconnectioninfo],
        // in a lobby and not ingame
        lobby: [this.divinlobby, this.divconnectioninfo],
        dead: [this.divdead],
        voting: [this.divvote],
        ingame: [],
    }

    this.hideElement = (element) => { element.classList.add("d-none"); };
    this.showElement = (element) => { element.classList.remove("d-none"); };
    // Remove all validation information
    this.unvalidate = (element) => { element.classList.remove("is-valid"); element.classList.remove("is-invalid"); };
    // Mark validation as successful
    this.validate = (element) => { element.classList.add("is-valid"); element.classList.remove("is-invalid"); };
    // Mark validation as unsuccessful
    this.invalidate = (element) => { element.classList.add("is-invalid"); element.classList.remove("is-valid"); };

    this.showUi = (uiElements) => {
        this.uiElements.all.forEach(this.hideElement);
        uiElements.forEach(this.showElement);
    };

    this.showUi(this.uiElements.disconnected);
})();

const darktheme = new (function DarkTheme(styleElement) {
    this.element = styleElement;
    this.state = styleElement.disabled;
    this.update = () => { this.element.disabled = this.state; }
    this.toggle = () => { this.state = !this.state; this.update(); };
    this.getState = () => this.state;
    this.enable = () => { this.state = false; this.update(); };
    this.disable = () => { this.state = true; this.update(); };
})(UI.cssdarktheme);

const storeusername = "werewolf_username";
const ENTER_KEY = 13;

window.addEventListener("beforeunload", function (e) {
    if (connection.connectionState !== signalR.HubConnectionState.Connected) return undefined;
    const confirmationMessage = "You are still connected to your lobby!\nLeaving this page will be disconnected!";
    (e || window.event).returnValue = confirmationMessage; //Gecko + IE
    return confirmationMessage; //Gecko + Webkit, Safari, Chrome etc.
});

function closeHandler(error) {
    console.error(error);
    UI.lblconnectionstate.textContent = "You are not connected!";
    UI.showUi(UI.uiElements.disconnected);
    darktheme.disable();
    console.log("Retrying connection in 5 seconds");
    setTimeout(() => start(), 5000);
};

function openHandler() {
    console.log("SignalR Connected.");
    UI.lblconnectionstate.textContent = "You are connected. Please log in!";
    lobbyleft({ success: true }); // send successful lobby left maybe
    UI.showUi(UI.uiElements.connected);
    darktheme.disable();
    restoreUsername();
};

function restoreUsername() {
    // restore from sessionstorage before going to local storage to allow mutliple tabs with auto reconnect
    const sessionUsername = sessionStorage.getItem(storeusername);
    if (sessionUsername !== null) setUserName(sessionUsername);
    else {
        const localUsername = localStorage.getItem(storeusername);
        if (localUsername !== null) setUserName(localUsername);
    }
};

connection.onclose(closeHandler);

async function start() {
    try {
        await connection.start();
    } catch (err) {
        console.assert(connection.state === signalR.HubConnectionState.Disconnected);
        console.error(err);
        console.log("Retrying connection in 5 seconds");
        setTimeout(() => start(), 5000);
        return;
    }
    console.assert(connection.state === signalR.HubConnectionState.Connected);
    openHandler();
};

const alertLevel = {
    Danger: { id: 1, class: "alert-danger", text: "Error:" },
    Warning: { id: 2, class: "alert-warning", text: "Warning:" },
    Info: { id: 4, class: "alert-info", text: "Info:" },
    Success: { id: 8, class: "alert-success", text: "Success:" }
};

// Bootstrap dismissable altert
function bsalert(message, level) {
    UI.divalert.insertAdjacentHTML("beforeend", `<div class="alert ${level.class} alert-dismissible fade show" role="alert">
  <strong>${level.text}</strong> ${message}
  <button type="button" class="close" data-dismiss="alert" aria-label="Close">
    <span aria-hidden="true">&#215;</span>
  </button>
</div>`);
};

function bsdismiss() {
    UI.divalert.innerHTML = "";
};

function handleUnexpectedError(response) { bsalert(response.message, alertLevel.Danger); };

async function setUserName(requsername) {
    const response = await connection.invoke("ChangeUsername", requsername);
    if (response.success) {
        const newusername = response.result;
        UI.lblconnectionstate.textContent = `You are connected as ${newusername}!`;
        localStorage.setItem(storeusername, newusername);
        sessionStorage.setItem(storeusername, newusername);
        UI.tbxusername.value = newusername;
        UI.showUi(UI.uiElements.loggedin);
        darktheme.disable();

        UI.validate(UI.tbxusername);
    } else {
        // UI.Invalidate UI.tbxusername
        UI.lblloginfeedback.textContent = response.message;
        UI.invalidate(UI.tbxusername);
    }
};

function lobbyhandler(response) {
    if (response.success) {
        // successful request
        UI.tbxlobbyid.value = ""

        // joined lobby
        UI.lbllobby.textContent = `You are in lobby: ${response.result}!`
        UI.validate(UI.tbxlobbyid);
        // hide lobby feedback on div
        UI.lbllobbyfeedback.classList.remove("d-block");

        // Hide username login when in lobby (visually enforce protocol restriction)
        UI.showUi(UI.uiElements.lobby);
        darktheme.disable();
    } else {
        // error in request
        UI.lbllobbyfeedback.textContent = response.message;
        UI.invalidate(UI.tbxlobbyid);
        // show lobby feedback on div
        UI.lbllobbyfeedback.classList.add("d-block");
    }
};

UI.btnchangeuser.addEventListener("click", () => setUserName(UI.tbxusername.value));

UI.btncreatelobby.addEventListener("click", async () => lobbyhandler(await connection.invoke("CreateLobby")));
UI.btnjoinlobby.addEventListener("click", async () => lobbyhandler(await connection.invoke("JoinLobby", UI.tbxlobbyid.value)));

UI.btnvoteplayer.addEventListener("click", async () => {
    const response = await connection.invoke("VoteForPlayer", UI.cbxvoteoptions.value);
    if (response.success)
        UI.showUi(UI.uiElements.ingame);
    else
        handleUnexpectedError(response);
});

UI.btnstartgame.addEventListener("click", async () => {
    // TODO: hide when host
    const result = await connection.invoke("StartGame");
    if (!result.success) bsalert(result.message, alertLevel.Warning);
});

async function lobbyleft(response) {
    if (response.success) {
        // lobby left
        UI.lbllobby.textContent = "You are not in a lobby!";
        UI.unvalidate(UI.tbxlobbyid);
        // hide lobby feedback on div
        UI.lbllobbyfeedback.classList.remove("d-block");

        // Show username login when not lobby
        UI.showUi(UI.uiElements.loggedin);
        darktheme.disable();
    } else {
        bsalert(response.message, alertLevel.Danger);
    }
};

UI.btnleavelobby.addEventListener("click", async () => lobbyleft(await connection.invoke("LeaveLobby")));

// Add enter key functionality to textbox input group 
UI.tbxusername.addEventListener("keyup", function (event) {
    if (event.keyCode === ENTER_KEY) {
        event.preventDefault();
        UI.btnchangeuser.click();
    }
});
UI.tbxusername.addEventListener("input", () => UI.unvalidate(UI.tbxusername));

// Add enter key functionality to textbox input group button
UI.tbxlobbyid.addEventListener("keyup", function (event) {
    if (event.keyCode === ENTER_KEY) {
        event.preventDefault();
        UI.btnjoinlobby.click();
    }
});

// Set lobby input to uppercase only
UI.tbxlobbyid.addEventListener("input", function () {
    UI.unvalidate(UI.tbxlobbyid);
    // hide lobby feedback on unvalidation
    UI.lbllobbyfeedback.classList.remove("d-block");
    let p = this.selectionStart;
    this.value = this.value.toUpperCase();
    this.setSelectionRange(p, p);
});

connection.on("GameStarted", function () {
    console.info("Game started!");
    UI.showUi(UI.uiElements.ingame);
});

connection.on("Message", function (message) {
    bsalert(message, alertLevel.Info);
});

connection.on("RoleAssigned", function (role) {
    bsalert(`You are now a ${role}!`, alertLevel.Success);
});

connection.on("Day", function (updates) {
    darktheme.disable();
    console.log(updates);
});

connection.on("Night", function (updates) {
    darktheme.enable();
    console.log(updates);
});

connection.on("Awake", function (message, usernames, timout) {
    UI.cbxvoteoptions.innerHTML = usernames.map(i => `<option value="${i}">${i}</option>`).join("");
    UI.showUi(UI.uiElements.voting);
    // TODO: display timeout
});

connection.on("Sleep", function (message, voteCounted) {
    UI.showUi(UI.uiElements.ingame);
    UI.cbxvoteoptions.innerHTML = "";
    bsalert(message, voteCounted ? alertLevel.Success : alertLevel.Warning);
    if (!voteCounted) console.info("Vote was not counted!");
});

connection.on("Died", function () {
    UI.showUi(UI.uiElements.dead);
});

connection.on("GameOver", function (winners, reason) {
    UI.showUi(UI.uiElements.lobby);
    console.log(winners);
    bsalert(reason, alertLevel.Info);
});

start();