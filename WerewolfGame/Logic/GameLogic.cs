﻿using System;
using System.Collections.Generic;
using System.Diagnostics.CodeAnalysis;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using WerewolfGame.Hubs;
using WerewolfGame.Hubs.Interfaces;
using WerewolfGame.Logic.Roles;

namespace WerewolfGame.Logic {
    /// <summary>
    /// Provides the class that handles game logic.
    /// One instance corresponds to one execution of a game.
    /// A lobby must create a new gamelogic to run another round.
    /// </summary>
    public class GameLogic {
        /// <summary>
        /// Thread that is used to execute the main game loop.
        /// </summary>
        private Thread? worker;
        /// <inheritdoc cref="Voting"/>
        private Voting? voting;
        /// <summary>
        /// Voting object that represents ongoing votes or null if no vote is active.
        /// </summary>
        public Voting? Voting { get => voting; }
        /// <summary>
        /// List of members of the game round.
        /// </summary>
        private readonly HashSet<User> members;
        /// <summary>
        /// Gets the client that corresponds to the interface to all active membrs.
        /// </summary>
        private IGameClient Connected => members.GetClient();

        /// <summary>
        /// Thread that is used to execute the main game loop.
        /// </summary>
        public event EventHandler<LobbyStatus>? StateChanged;

        /// <summary>
        /// Construct a new game logic instance.
        /// </summary>
        /// <param name="members">Currently connected members.</param>
        public GameLogic(HashSet<User> members) {
            this.members = members;
        }

        /// <summary>
        /// Start the game loop for the given lobby members.
        /// </summary>
        public Task<Maybe> Start() {
            if (worker is not null && worker.ThreadState == ThreadState.Running)
                throw new InvalidOperationException("Game logic thread is busy!");
            var profile = GetProfile(members.Count);
            worker = new Thread(GameLoop);
            worker.Start(profile);
            return Task.FromResult(Maybe.Successful);
        }

        /// <summary>
        /// The main game loop that runs in the thread.
        /// </summary>
        private async void GameLoop(object? obj) {
            StateChanged?.Invoke(this, LobbyStatus.Playing);
            IDictionary<User, GameRole> alive;
            IDictionary<User, GameRole> dead = new Dictionary<User, GameRole>();
            IDictionary<User, GameRole> newdead = new Dictionary<User, GameRole>();
            IEnumerable<PlayerUpdate> updates;
            User? chosenPlayer;
            if (obj is not Profile profile) throw new InvalidProgramException("Game loop started without setting profile!");
            lock (members)
                alive = profile.AssignRoles(members);

            await PublishRoles(alive);
            string? reason;
            try {
                while (true) {
                    // Nightphase
                    updates = await HandlePlayerUpdates(dead, newdead);
                    if (IsGameOver(alive, out reason)) break;

                    await Connected.Night(updates);
                    chosenPlayer = await WerewolfPhase(alive);
                    if (chosenPlayer is not null) KillPlayer(alive, newdead, chosenPlayer);

                    // Day phase
                    updates = await HandlePlayerUpdates(dead, newdead);
                    if (IsGameOver(alive, out reason)) break;

                    await Connected.Day(updates);
                    chosenPlayer = await VillagerPhase(alive);
                    if (chosenPlayer is not null) KillPlayer(alive, newdead, chosenPlayer);
                }
                await Connected.GameOver(alive, reason);
            } catch (Exception e) {
                await Connected.GameOver(alive, "The game was aborted due to an unexpected error!");
                Console.WriteLine(e.ToString());
                throw;
            } finally {
                StateChanged?.Invoke(this, LobbyStatus.Waiting);
            }
        }

        /// <summary>
        /// Checks if the game is over based on the currently alive players.
        /// If its game over the reason will contain the gameover message.
        /// </summary>
        /// <param name="alive">Currently alive players</param>
        /// <param name="reason">The reason why it's game over or <c>null</c> if it is not</param>
        /// <returns><c>true</c> iff it's game over</returns>
        private static bool IsGameOver(IDictionary<User, GameRole> alive, [NotNullWhen(true)] out string? reason) {
            var aliveroles = alive.Values.ToList();
            var werewolves = aliveroles.Count(i => i is Werewolf);
            if (werewolves == 0) {
                reason = "The villagers have killed Connected werewolves!";
                return true;
            }
            var others = aliveroles.Count - werewolves;
            if (werewolves >= others) {
                reason = "The werewolves have outnumbered the villagers!";
                return true;
            }
            reason = null;
            return false;
        }

        /// <summary>
        /// Create a game configuration profile for the given amount of players.
        /// </summary>
        /// <returns>Profile that represents the role assignment rules for this game.</returns>
        /// <exception cref="ArgumentException">Invalid amount of players.</exception>
        private static Profile GetProfile(int memberCount) {
            return new Profile()
                .Add<Seer>()
                .Add<Werewolf>(memberCount switch {
#if DEBUG
                    // TODO: Remove debug profile and throw proper error
                    _ when memberCount >= 2 && memberCount < 6 => 1,
#endif
                    _ when memberCount >= 6 && memberCount < 9 => 1,
                    _ when memberCount >= 9 && memberCount < 12 => 2,
                    _ when memberCount >= 12 && memberCount < 16 => 3,
                    _ => throw new ArgumentException($"Cannot create profile for {memberCount} player{(memberCount == 1 ? string.Empty : "s")}!")
                });
        }

        /// <summary>
        /// Sends Connected recently died people to the list of dead people and clear the recents list.
        /// </summary>
        /// <param name="dead">List of users that died earlier.</param>
        /// <param name="newdead">List of users that died this round</param>
        private static async Task MoveDeadToGraveyard(IDictionary<User, GameRole> dead, IDictionary<User, GameRole> newdead) {
            await newdead.Keys.GetClient().Died();
            foreach (var user in newdead) dead.Add(user.Key, user.Value);
            newdead.Clear();
        }

        /// <summary>
        /// Get a list of player updates from the list of recently died players and  
        /// sends Connected recently died people to the list of dead people and clear the recents list.
        /// </summary>
        /// <param name="dead">List of users that died earlier.</param>
        /// <param name="newdead">List of users that died this round</param>
        /// <returns>Enumerable of PlayerUpdates</returns>
        private static async Task<IEnumerable<PlayerUpdate>> HandlePlayerUpdates(IDictionary<User, GameRole> dead, IDictionary<User, GameRole> newdead) {
            await MoveDeadToGraveyard(dead, newdead);
            return newdead.Select(i => new PlayerUpdate($"{i.Key.Username} was a {i.Value}!", i.Key.Username));
        }

        /// <summary>
        /// Kill the player.
        /// </summary>
        /// <param name="alive">Currently alive users with their roles</param>
        /// <param name="dead">List of users that died this round</param>
        /// <param name="killuser">User to move from alive to dead list</param>
        /// <exception cref="InvalidProgramException">Killed user not in <paramref name="alive"/> dict</exception>
        private static void KillPlayer(IDictionary<User, GameRole> alive, IDictionary<User, GameRole> dead, User killuser) {
            lock (alive) {
                if (!alive.TryGetValue(killuser, out GameRole? deaduserrole))
                    throw new InvalidProgramException("Killed user not in roles dict!");
                alive.Remove(killuser);
                dead[killuser] = deaduserrole;
            }
        }

        /// <summary>
        /// Start the werewolf phase and let them vote for a player to kill.
        /// </summary>
        /// <param name="alive">Dictionary of player roles</param>
        /// <returns>User that is supposed to be killed</returns>
        private async Task<User?> WerewolfPhase(IDictionary<User, GameRole> alive) {
            await Connected.Message("The werewolves awaken and shall pick their victim!");
            voting = new Voting("Who shall be your victim?",
                alive.UsersByRole<Werewolf>(),
                alive.UsersByRole<GameRole>(i => i is not Werewolf).Usernames());
            var voteResult = (await voting.CollectVotes(60)).ToList();
            // TODO: handle ambiguous by revote or noop.
            var killUsername = voteResult.First();
            return alive.Keys.First(i => i.Username == killUsername) ?? throw new InvalidProgramException("Username is not existent!");
        }


        /// <summary>
        /// Start the villager phase and let them vote for a player to kill.
        /// </summary>
        /// <param name="alive">Dictionary of player roles</param>
        /// <returns>User that is supposed to be killed</returns>
        private async Task<User?> VillagerPhase(IDictionary<User, GameRole> alive) {
            await Connected.Message("Connected villagers come together to decide who to lynch!");
            voting = new Voting("Who shall be lynched?",
                alive.Keys,
                alive.Keys.Select(i => (string)i.Username));
            var voteResult = (await voting.CollectVotes(60)).ToList();
            // TODO: handle ambiguous by revote or noop.
            var killUsername = voteResult.First();
            return alive.Keys.First(i => i.Username == killUsername) ?? throw new InvalidProgramException("Username is not existent!");
        }

        /// <summary>
        /// Show to each user only their own role.
        /// </summary>
        /// <param name="roles">Dictionary of assigned roles</param>
        private static async Task PublishRoles(IDictionary<User, GameRole> roles)
            => await Task.WhenAll(roles.SelectOnline((client, kvp) => client.RoleAssigned(kvp.Name)));
    }
}