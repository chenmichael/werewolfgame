﻿namespace WerewolfGame.Logic.Roles {
    /// <summary>
    /// Provides an abstract of possible roles a player can assume.
    /// This class is responsible for handling game logic and role functionality/skills.
    /// </summary>
    public abstract class GameRole {
        /// <summary>
        /// Gets the name of the game role.
        /// </summary>
        public abstract string Name { get; }
    }

    /// <summary>
    /// Normal players without special skills.
    /// </summary>
    public class Villager : GameRole {
        /// <inheritdoc/>
        public override string Name => nameof(Villager);
    }

    /// <summary>
    /// Werewolfes that can vote to kill one player during night time.
    /// </summary>
    public class Werewolf : GameRole {
        /// <inheritdoc/>
        public override string Name => nameof(Werewolf);
    }

    /// <summary>
    /// Seer that can request to get information about another players during night time.
    /// </summary>
    public class Seer : GameRole {
        /// <inheritdoc/>
        public override string Name => nameof(Seer);
    }

    /// <summary>
    /// The witch has two potions and can during the entire game revive one player and kill one player.
    /// </summary>
    public class Witch : GameRole {
        /// <inheritdoc/>
        public override string Name => nameof(Witch);
    }
}