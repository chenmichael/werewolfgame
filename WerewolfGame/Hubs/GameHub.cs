﻿using Microsoft.AspNetCore.SignalR;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using WerewolfGame.Hubs.Interfaces;

namespace WerewolfGame.Hubs {
    /// <summary>
    /// Provides the strongly typed SignalR Hub class that handles all the client SignalR connection calls.
    /// </summary>
    public class GameHub : Hub<IGameClient>, IGameServer {
        private static readonly Dictionary<string, User> online = new Dictionary<string, User>();
        private static readonly Dictionary<LobbyId, Lobby> lobbies = new Dictionary<LobbyId, Lobby>();
        /// <summary>
        /// Gets the default path that this hub shall be mapped to during startup (see <see cref="Startup"/>).
        /// </summary>
        public const string DefaultPath = "/game";
        private const string UserItem = "USR";
        private User User {
            get => (Context.Items[UserItem] as User) ?? throw new ArgumentException("You are not logged in. Set your username first!");
            set => Context.Items[UserItem] = value;
        }

        #region Connection Handling
        /// <inheritdoc cref="Hub.OnConnectedAsync()"/>
        public override async Task OnConnectedAsync() {
            await base.OnConnectedAsync();
        }

        /// <inheritdoc cref="Hub.OnDisconnectedAsync(Exception?)"/>
        public override async Task OnDisconnectedAsync(Exception? exception) {
            User? user;
            lock (online)
                online.Remove(Context.ConnectionId, out user);
            if (user is not null)
                await user.OnDisconnectedAsync();
            await base.OnDisconnectedAsync(exception);
        }
        #endregion

        #region Gameserver implementation
        /// <inheritdoc cref="IGameServer.ChangeUsername(string)"/>
        public Maybe<Username> ChangeUsername(string username) {
            User user;
            try {
                user = User;
                // User is logged in => assign new name
                return AssignUsernameSafe(username, user);
            } catch (ArgumentException) {
                // Not logged in yet
                return LoginNewUser(username);
            }
        }

        /// <inheritdoc cref="IGameServer.CreateLobby()"/>
        public async Task<Maybe<LobbyId>> CreateLobby() {
            try {
                var user = User;
                var lobby = user.CurrentLobby;
                if (lobby is not null) throw new ArgumentException("You cannot join multiple lobbies simultaneosly!");
                lock (lobbies) {
                    lobby = new Lobby(LobbyId.GenerateUnusedLobbyID(lobbies.Keys.ToHashSet()));
                    lobby.LobbyClosed += LobbyClosed;
                    lobbies.Add(lobby.ID, lobby);
                }
                return await lobby.Join(user);
            } catch (ArgumentException e) {
                return e;
            }
        }

        /// <inheritdoc cref="IGameServer.JoinLobby(string)"/>
        public async Task<Maybe<LobbyId>> JoinLobby(string lobbyid) {
            try {
                var id = new LobbyId(lobbyid);
                var user = User;
                if (user.CurrentLobby is not null) throw new ArgumentException($"You are already in lobby '{user.CurrentLobby.ID}'!");
                Lobby? lobby;
                lock (lobbies)
                    if (!lobbies.TryGetValue(id, out lobby))
                        throw new ArgumentException($"Lobby '{id}' doesn't exist!");
                return await lobby.Join(user);
            } catch (ArgumentException e) {
                return e;
            }
        }

        /// <inheritdoc cref="IGameLobby.LeaveLobby()"/>
        public async Task<Maybe> LeaveLobby() {
            try {
                var user = User;
                if (user is not null) {
                    if (user.CurrentLobby is not null)
                        return await user.LeaveLobby();
                }

                return Maybe.Successful;
            } catch (ArgumentException e) {
                return e;
            }
        }

        /// <inheritdoc cref="IGameLobby.VoteForPlayer(string)"/>
        public async Task<Maybe> VoteForPlayer(string username) {
            try {
                var user = User;
                return await user.VoteForPlayer(username);
            } catch (ArgumentException e) {
                return e;
            }
        }

        /// <inheritdoc cref="IGameLobby.StartGame()"/>
        public async Task<Maybe> StartGame() {
            try {
                var user = User;
                return await user.StartGame();
            } catch (ArgumentException e) {
                return e;
            }
        }
        #endregion

        #region Helper functions
        /// <summary>
        /// Assigns the new <paramref name="username"/> to the given <paramref name="user"/>.
        /// This method performs username validation for you <see cref="Username.ValidateUsername(string)"/>.
        /// If the username is already taken nothing changes.
        /// Do not lock <see cref="online"/>!
        /// </summary>
        /// <param name="username">Username to assign</param>
        /// <param name="user">User to assign the <paramref name="username"/> to</param>
        private static Maybe<Username> AssignUsernameSafe(string username, User user) {
            try {
                if (user.CurrentLobby is not null)
                    throw new ArgumentException("Cannot change username while in a lobby!");
                var name = new Username(username);
                if (user.Username != name)
                    // If user chose a different name check if it is available
                    lock (online) {
                        if (IsUsernameTaken(name))
                            throw new ArgumentException($"Username '{name}' is already taken!");
                    }
                user.Username = name;
                var val = Newtonsoft.Json.JsonConvert.SerializeObject(user.Username);
                return user.Username;
            } catch (ArgumentException e) {
                return e;
            }
        }

        /// <summary>
        /// Create the user object for a new user and register the user in the member list.
        /// </summary>
        /// <param name="username">Username to select for the user</param>
        private Maybe<Username> LoginNewUser(string username) {
            try {
                var name = new Username(username);
                User user;
                lock (online) {
                    if (IsUsernameTaken(name))
                        throw new ArgumentException($"Username '{name}' is already taken!");
                    user = new User(name) { ConnectionId = Context.ConnectionId };
                    online.Add(Context.ConnectionId, user);
                }
                User = user;
                return user.Username;
            } catch (ArgumentException e) {
                return e;
            }
        }

        /// <summary>
        /// Event handler that is called when a lobby is closed. Will remove the lobby from the list of lobbies.
        /// </summary>
        /// <param name="sender">Lobby that raised its close event.</param>
        /// <param name="e">Event arguments.</param>
        private void LobbyClosed(object? sender, EventArgs e) {
            if (sender is not Lobby lobby) throw new InvalidProgramException("Lobby reference expected!");
            lock (lobbies)
                lobbies.Remove(lobby.ID);
        }

        /// <summary>
        /// Checks wheter a username is already used by a session.
        /// Requires lock on <see cref="online"/>.
        /// </summary>
        /// <param name="username">Username to check for.</param>
        /// <returns><c>true</c> iff the username is taken.</returns>
        private static bool IsUsernameTaken(Username username) {
            return online.Values.Any(i => i.Username == username);
        }
        #endregion
    }
}
