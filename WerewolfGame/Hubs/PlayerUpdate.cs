﻿namespace WerewolfGame.Hubs {
#pragma warning disable CS1591 // Missing XML comment for publicly visible type or member
    // TODO: extend
    public class PlayerUpdate {
        public PlayerUpdate(string message, string username) {
            Message = message;
            Username = username;
        }
        public string Message { get; set; }
        public string Username { get; set; }
    }
#pragma warning restore CS1591 // Missing XML comment for publicly visible type or member
}