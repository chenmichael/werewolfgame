﻿using Newtonsoft.Json;
using System;

namespace WerewolfGame.Hubs {
    /// <summary>
    /// Instructs the json serializer to serialize the object as its string representation.
    /// </summary>
    public class ToStringJsonConverter : JsonConverter {
        /// <inheritdoc/>
        public override bool CanConvert(Type objectType) => true;
        /// <inheritdoc/>
        public override bool CanRead => false;
        /// <inheritdoc/>
        public override void WriteJson(JsonWriter writer, object value, JsonSerializer serializer)
            => writer.WriteValue(value.ToString());
        /// <inheritdoc/>
        public override object ReadJson(JsonReader reader, Type objectType, object existingValue, JsonSerializer serializer)
            => throw new NotImplementedException();
    }
}