﻿namespace WerewolfGame {
    /// <summary>
    /// Enumeration of possible game states of a lobby.
    /// </summary>
    public enum LobbyStatus {
        /// <summary>
        /// Lobby is currently waiting to start and accepting members.
        /// </summary>
        Waiting,
        /// <summary>
        /// Lobby is currently in active game session.
        /// </summary>
        Playing,
        /// <summary>
        /// Lobby is closed.
        /// </summary>
        Closed
    }
}