﻿using Newtonsoft.Json;
using System;

namespace WerewolfGame.Hubs {
    /// <summary>
    /// Provides a class for sending a result that might be an exception or the result.
    /// </summary>
    /// <typeparam name="T">Type of successful result</typeparam>
    public class Maybe<T> : Maybe {
        /// <summary>
        /// Construct a new successful result from the input.
        /// </summary>
        /// <param name="res">Result to represent</param>
        public Maybe(T res) : base() => Result = res;

        /// <summary>
        /// Construct a new failed result from the exception.
        /// </summary>
        /// <param name="ex">Exception to construct</param>
        public Maybe(Exception ex) : base(ex) { }

        /// <summary>
        /// Object that stores the actual result.
        /// </summary>
        [JsonProperty("result")]
        public T? Result { get; }
        /// <summary>
        /// Whether to serialize the result parameter.
        /// </summary>
        public bool ShouldSerializeResult() => Success;

        /// <summary>
        /// Implicitly cast the maybe to a value. If the result was an error, that exact error is thrown.
        /// </summary>
        /// <param name="res">Maybe to cast to its value</param>
        /// <exception cref="Exception">Maybe was created from an error</exception>
        /// <exception cref="InvalidProgramException">Neither result nor error had a value</exception>
        public static implicit operator T(Maybe<T> res) => res.Result ?? throw res.Error ?? throw new InvalidProgramException("Error was not defined!");

        /// <summary>
        /// Implicitly cast a value to its maybe.
        /// </summary>
        /// <param name="res">Value to cast to its maybe</param>
        public static implicit operator Maybe<T>(T res) => new Maybe<T>(res);

        /// <summary>
        /// Implicitly cast an exception to its maybe.
        /// </summary>
        /// <param name="ex">Exception to cast to its maybe</param>
        public static implicit operator Maybe<T>(Exception ex) => new Maybe<T>(ex);
    }

    /// <summary>
    /// Provides a class for sending a success message or an exception.
    /// </summary>
    public class Maybe {
        /// <summary>
        /// Returns a successful maybe result.
        /// </summary>
        public static readonly Maybe Successful = new Maybe();

        /// <summary>
        /// Construct a new succcessful result.
        /// </summary>
        protected Maybe() => Success = true;

        /// <summary>
        /// Construct a new failed result from the exception.
        /// </summary>
        /// <param name="ex">Exception to construct</param>
        public Maybe(Exception ex) {
            Error = ex;
            Success = false;
        }

        /// <summary>
        /// Error object that was raised.
        /// </summary>
        [JsonIgnore]
        public Exception? Error { get; }

        /// <summary>
        /// Boolean that shows if the result is successful.
        /// If <c>true</c> the Result is not null.
        /// If <c>false</c> the error message is not null.
        /// </summary>
        [JsonProperty("success")]
        public bool Success { get; }

        /// <summary>
        /// String representation of the error.
        /// </summary>
        [JsonProperty("message")]
        public string? Message { get => Error?.Message; }
        /// <summary>
        /// Whether to serialize the message parameter.
        /// </summary>
        public bool ShouldSerializeMessage() => !Success;

        /// <summary>
        /// Implicitly cast a value to its maybe.
        /// </summary>
        /// <param name="ex">Value to cast to its maybe</param>
        public static implicit operator Maybe(Exception ex) => new Maybe(ex);
    }
}