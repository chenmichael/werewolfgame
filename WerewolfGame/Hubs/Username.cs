﻿using Newtonsoft.Json;
using System;
using System.Diagnostics;
using System.Linq;

namespace WerewolfGame.Hubs {
    /// <summary>
    /// Provides a class for validated usernames and comparison.
    /// </summary>
    [JsonConverter(typeof(ToStringJsonConverter))]
    [DebuggerDisplay("{" + nameof(ToString) + "(),nq}")]
    public class Username {
        private readonly string rep;

        /// <summary>
        /// Constructs a new username from the string repsresentation.
        /// </summary>
        /// <param name="name">String representation of a username</param>
        /// <exception cref="ArgumentException">Username is invalid (see <see cref="ValidateUsername(string)"/>)</exception>
        public Username(string name) {
            rep = ValidateUsername(name);
        }

        /// <summary>
        /// Checks if a username is valid and returns it.
        /// If it is possible to fix errors in the username the corrected name is returned.
        /// </summary>
        /// <param name="username">Input username.</param>
        /// <exception cref="ArgumentException"><paramref name="username"/> is invalid.</exception>
        /// <returns>Corrected (if possible) name.</returns>
        private static string ValidateUsername(string username) {
            username = username.Trim();
            if (username.Length < 3 || username.Length > 24) throw new ArgumentException("Username must be between 3 and 24 characters!");
            if (!username.All(c => c == ' ' || char.IsLetterOrDigit(c))) throw new ArgumentException("Username must only contain letters, digits and spaces.");
            if (username.Contains("  ")) throw new ArgumentException("Username must not contain two successive spaces.");
            return username;
        }

        /// <inheritdoc/>
        public static bool operator ==(Username? lhs, Username? rhs) {
            if (rhs is null) return lhs is null;
            if (lhs is null) return false;
            return string.Compare(lhs.rep, rhs.rep, true) == 0;
        }

        /// <inheritdoc/>
        public static bool operator !=(Username? lhs, Username? rhs) => !(lhs == rhs);

        /// <inheritdoc/>
        public override string ToString() => rep;

        /// <inheritdoc/>
        public override bool Equals(object? obj) => this == (obj as Username);

        /// <inheritdoc/>
        public override int GetHashCode() => rep.GetHashCode();

        /// <summary>
        /// Provides an implicit conversion from a username to its string representation.
        /// </summary>
        /// <param name="name">Username to convert implicitly</param>
        public static implicit operator string(Username name) => name.ToString();
    }
}
