﻿using System.Collections.Generic;
using System.Threading.Tasks;
using WerewolfGame.Logic.Roles;

namespace WerewolfGame.Hubs.Interfaces {
    /// <summary>
    /// Provides an interface for game hub methods that the server can invoke on clients.
    /// </summary>
    public interface IGameClient {
        /// <summary>
        /// Called to send game status messages to the users.
        /// The client must not answer this message.
        /// </summary>
        /// <param name="message">Message to display.</param>
        Task Message(string message);
        /// <summary>
        /// Notifies the players that the game has started.
        /// Response to <see cref="IGameLobby.StartGame()"/>.
        /// </summary>
        Task GameStarted();
        /// <summary>
        /// Awake a player to expect a player selection from the player for game actions.
        /// This is not a response but a server active message to request player choice.
        /// The client must answer with <see cref="IGameLobby.VoteForPlayer(string)"/>.
        /// </summary>
        /// <param name="message">Query message to display to the user.</param>
        /// <param name="usernames">List of users to select from.</param>
        /// <param name="timeout">Time in seconds the user has to respond to this awaking.</param>
        Task Awake(string message, IEnumerable<string> usernames, int timeout);
        /// <summary>
        /// Notify a player that the current voting was closed and can no longer be
        /// called using the <see cref="IGameLobby.VoteForPlayer(string)"/> method.
        /// This is not a response but a server active message.
        /// The client must not respond to this message.
        /// </summary>
        /// <param name="message">Message to display to the user about his timeout.</param>
        /// <param name="voteCounted">Boolean that specifies whether the users vote was counted.</param>
        // TODO: Return voting result to voters only
        // Can additionally return player info to seer
        Task Sleep(string message, bool voteCounted);
        /// <summary>
        /// Notifies a player that the night game phase started.
        /// It also sends a list of status <paramref name="updates"/> that happened during the day phase.
        /// The update list will of course be empty at the beginning of the first night.
        /// The client must not respond to this message.
        /// </summary>
        /// <param name="updates">Updates that happened during the day phase.</param>
        Task Night(IEnumerable<PlayerUpdate> updates);
        /// <summary>
        /// Notifies a player that the day game phase started.
        /// It also sends a list of status <paramref name="updates"/> that happened during the night phase.
        /// The client must not respond to this message.
        /// </summary>
        /// <param name="updates">Updates that happened during the night phase.</param>
        Task Day(IEnumerable<PlayerUpdate> updates);
        /// <summary>
        /// Notifies a player about his role in the game.
        /// The client must not respond to this message.
        /// </summary>
        /// <param name="role">Role that the player assumes in this game</param>
        Task RoleAssigned(string role);
        /// <summary>
        /// Notifies a player about his death.
        /// The client must not respond to this message.
        /// </summary>
        // TODO: add death info (who killed you how)
        Task Died();
        /// <summary>
        /// Notifies a player about the end of the lobbies game.
        /// The client must not respond to this message.
        /// </summary>
        /// <param name="reason">The reason why the game ended</param>
        /// <param name="winners">The list of winners of the game</param>
        // TODO: add death info (who killed you how)
        Task GameOver(IDictionary<User, GameRole> winners, string reason);
    }
}