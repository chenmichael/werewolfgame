﻿using System;
using System.Threading.Tasks;

namespace WerewolfGame.Hubs.Interfaces {
    /// <summary>
    /// Interface for the game lobby. Includes methods that clients cann call inside a game lobby..
    /// </summary>
    public interface IGameLobby {
        /// <summary>
        /// Choose the user with the given <paramref name="username"/> during votes or game actions.
        /// </summary>
        /// <exception cref="ArgumentException"><paramref name="username"/> is not selectable.</exception>
        /// <exception cref="ArgumentException">No selection is online.</exception>
        /// <exception cref="ArgumentException">Already chosen.</exception>
        /// <exception cref="ArgumentException">Not in a lobby.</exception>
        /// <param name="username">Username of player to select.</param>
        /// <returns>Optional error message</returns>
        Task<Maybe> VoteForPlayer(string username);
        /// <summary>
        /// Leave the current lobby. If not in a lobby nothing happens.
        /// </summary>
        /// <returns>Returns no response. Leaving is always successful.</returns>
        // TODO: Check leave when in-game
        Task<Maybe> LeaveLobby();
        /// <summary>
        /// Starts the game for the current lobby.
        /// </summary>
        /// <exception cref="ArgumentException">Not in a lobby.</exception>
        /// <exception cref="ArgumentException">Insufficient permissions.</exception>
        /// <exception cref="ArgumentException">Not enough players.</exception>
        /// <returns>Optional error message</returns>
        Task<Maybe> StartGame();
    }
}