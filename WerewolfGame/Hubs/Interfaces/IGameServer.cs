﻿using System;
using System.Threading.Tasks;

namespace WerewolfGame.Hubs.Interfaces {
    /// <summary>
    /// Interface for the game server. Includes methods that all clients can call.
    /// </summary>
    public interface IGameServer : IGameLobby {
        /// <summary>
        /// Initialize or change the username of the current connection to <paramref name="username"/>.
        /// The only method that can be used when not logged in.
        /// </summary>
        /// <exception cref="ArgumentException"><paramref name="username"/> is invalid. See <see cref="Username.ValidateUsername(string)"/>.</exception>
        /// <exception cref="ArgumentException"><paramref name="username"/> is already taken.</exception>
        /// <param name="username">Username to select.</param>
        /// <returns>Username or exception message.</returns>
        Maybe<Username> ChangeUsername(string username);
        /// <summary>
        /// Create a lobby and join as host.
        /// </summary>
        /// <exception cref="ArgumentException">Already member of another lobby.</exception>
        /// <exception cref="ArgumentException">Not logged in (see <see cref="ChangeUsername(string)"/>).</exception>
        /// <returns>Lobby ID or exception message.</returns>
        Task<Maybe<LobbyId>> CreateLobby();
        /// <summary>
        /// Join the lobby with the given <paramref name="lobbyid"/>.
        /// </summary>
        /// <param name="lobbyid">Lobby to join.</param>
        /// <exception cref="ArgumentException">Already member of another lobby.</exception>
        /// <exception cref="ArgumentException">Lobby doesn't exist.</exception>
        /// <exception cref="ArgumentException">Not logged in (see <see cref="ChangeUsername(string)"/>).</exception>
        /// <exception cref="InvalidOperationException">Lobby doesn't accept members.</exception>
        /// <returns>Lobby ID or exception message.</returns>
        Task<Maybe<LobbyId>> JoinLobby(string lobbyid);
    }
}