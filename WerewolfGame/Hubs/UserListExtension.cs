﻿using System;
using System.Collections.Generic;
using System.Linq;
using WerewolfGame.Hubs.Interfaces;
using WerewolfGame.Logic.Roles;

namespace WerewolfGame.Hubs {
    /// <summary>
    /// Static class that provides extension methods for lists of <see cref="User"/>s.
    /// </summary>
    public static class UserListExtension {
        /// <summary>
        /// Gets the <see cref="IGameClient"/> that corresponds to all of the online users in the collection.
        /// </summary>
        /// <param name="users"></param>
        /// <returns></returns>
        public static IGameClient GetClient(this IEnumerable<User> users)
            => Program.HubContext.Clients.Clients(users.GetOnline());

        /// <summary>
        /// Gets the list of connection ids of all currently online users in the collection.
        /// </summary>
        /// <param name="users"></param>
        /// <returns></returns>
        public static IReadOnlyList<string> GetOnline(this IEnumerable<User> users)
            => users.Select(i => i.ConnectionId)
            .Where(i => i is not null)
            .Select(i => i!)
            .ToList()
            .AsReadOnly();

        /// <summary>
        /// Gets a list of usernames from a list of users.
        /// </summary>
        /// <param name="users">List of users</param>
        /// <returns>Enumerator over the users' names</returns>
        public static IEnumerable<string> Usernames(this IEnumerable<User> users)
            => users.Select(i => (string)i.Username);

        /// <summary>
        /// Evaluates the function for every online client and returns the results (ie. Tasks).
        /// </summary>
        /// <param name="users">List of users</param>
        /// <param name="func">Function to calculate</param>
        /// <returns>Enumerator over the return values</returns>
        public static IEnumerable<R> SelectOnline<R, T>(this IDictionary<User, T> users, Func<IGameClient, T, R> func)
            => users.Where(i => i.Key.Client is not null).Select(i => func(i.Key.Client!, i.Value));

        /// <summary>
        /// Enumerates over Connected users that have a specific role.
        /// </summary>
        /// <param name="users">Mapping of users to their roles</param>
        /// <returns>Enumerator over the users</returns>
        public static IEnumerable<User> UsersByRole<T>(this IDictionary<User, GameRole> users) where T : GameRole
            => users.UsersByRole<T>(_ => true);

        /// <summary>
        /// Enumerates over Connected users that have a role and for that the <paramref name="filter"/> matches.
        /// </summary>
        /// <param name="users">Mapping of users to their roles</param>
        /// <param name="filter">Filter that maps roles to booleans</param>
        /// <returns>Enumerator over the users</returns>
        public static IEnumerable<User> UsersByRole<T>(this IDictionary<User, GameRole> users, Func<T, bool> filter) where T : GameRole
            => users.Where(i => i.Value is T).Where(i => filter((i.Value as T)!)).Select(i => i.Key);
    }
}
