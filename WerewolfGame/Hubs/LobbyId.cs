﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Text;
using System.Text.RegularExpressions;

namespace WerewolfGame.Hubs {
    /// <summary>
    /// Provides a class for verification of lobby-ids.
    /// </summary>
    [JsonConverter(typeof(ToStringJsonConverter))]
    [DebuggerDisplay("{" + nameof(ToString) + "(),nq}")]
    public record LobbyId {
        private readonly string rep;
        private const int LOBBY_ID_LENGTH = 3;
        private static readonly Regex acceptedLobby = new Regex(@"^(?<id>[a-zA-Z]{" + LOBBY_ID_LENGTH.ToString() + "})$", RegexOptions.Compiled);
        private static readonly Random rnd = new Random();

        /// <summary>
        /// Construct a lobby id from a string representation.
        /// </summary>
        /// <param name="id">String representation of the lobby id</param>
        /// <exception cref="ArgumentException">Lobby ID representation is invalid</exception>
        public LobbyId(string id) {
            rep = Validate(id);
        }

        /// <summary>
        /// Construct a random string of uppercase letters from A to Z of given <paramref name="length"/>.
        /// </summary>
        /// <param name="length">Length of the output string</param>
        /// <returns>Random fixed length string of uppercase letters</returns>
        private static string RandomString(int length = 3) {
            var sb = new StringBuilder(length);
            for (var i = 0; i < length; i++) sb.Append((char)('A' + rnd.Next() % 26));
            return sb.ToString();
        }

        /// <summary>
        /// Generates a random lobby name.
        /// </summary>
        /// <returns>A new lobby name that is not already picked.</returns>
        /// <exception cref="InvalidProgramException">Lobby ID generation is invalid</exception>
        [DebuggerStepThrough]
        private static LobbyId GenerateID() {
            try {
                return new LobbyId(RandomString(LOBBY_ID_LENGTH));
            } catch (ArgumentException e) {
                throw new InvalidProgramException("Lobby ID generation didn't produce a valid lobby ID!", e);
            }
        }

        /// <summary>
        /// Generates a random lobby id that is not yet used.
        /// </summary>
        /// <returns>A new lobby id that is not already picked.</returns>
        /// <param name="ids">Hashset of currently used lobby names</param>
        /// <exception cref="InvalidProgramException">Lobby ID generation is invalid</exception>
        public static LobbyId GenerateUnusedLobbyID(HashSet<LobbyId> ids) {
            LobbyId id;
            do id = GenerateID();
            while (ids.Contains(id));
            return id;
        }

        /// <summary>
        /// Validates a lobby id and returns its normalized format.
        /// Lobby ID must consist of <see cref="LOBBY_ID_LENGTH"/> characters between A and Z (case insensitive).
        /// The Lobby ID is normalized to all uppercase letters.
        /// </summary>
        /// <param name="id">Unnormalized lobby id</param>
        /// <returns>Normalized lobby id</returns>
        /// <exception cref="ArgumentException">Lobby ID representation is invalid</exception>
        private static string Validate(string id) {
            var match = acceptedLobby.Match(id);
            if (!match.Success) throw new ArgumentException("Invalid lobby id!");
            return match.Groups["id"].Value.ToUpper();
        }

        /// <summary>
        /// Provides an implicit conversion from a lobby ID to its string representation.
        /// </summary>
        /// <param name="id">Lobby id to convert implicitly</param>
        public static implicit operator string(LobbyId id) => id.ToString();

        /// <inheritdoc/>
        public override string ToString() => rep;
    }
}