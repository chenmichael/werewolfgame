﻿using System;
using System.Collections.Generic;
using System.Linq;
using WerewolfGame.Logic.Roles;

namespace WerewolfGame.Hubs {
    /// <summary>
    /// Provides a class that decides how to assign roles to players.
    /// </summary>
    public class Profile {
        /// <summary>
        /// The list of special roles to assign.
        /// Do not include villagers as they are assigned automatically.
        /// </summary>
        private readonly IList<GameRole> roles = new List<GameRole>();

        /// <summary>
        /// Creates an infinite list of roles to be asssigned. The first elements will be the special roles and the rest is filled with default roles.
        /// </summary>
        private IEnumerable<GameRole> GetRoles() {
            foreach (var role in roles) yield return role;
            while (true) yield return new Villager();
        }

        /// <summary>
        /// Creates a shuffled list of roles to be asssigned.
        /// </summary>
        private IEnumerable<GameRole> ShuffleRoles(int memberCount) {
            var rnd = new Random();
            return GetRoles().Take(memberCount).OrderBy(_ => rnd.Next());
        }

        /// <summary>
        /// Map roles to users randomly. The profile configuration decides how many roles to assign
        /// </summary>
        /// <param name="members">The currently online members to assign roles to</param>
        /// <returns>A list of role assignmets that map users to roles</returns>
        public IDictionary<User, GameRole> AssignRoles(HashSet<User> members)
            => members.Zip(ShuffleRoles(members.Count)).ToDictionary(i => i.First, i => i.Second);

        /// <summary>
        /// Provides a factory-like interface for building a profile.
        /// Adds the given role to the profile.
        /// </summary>
        /// <param name="amount">Amount of roles to add of this type</param>
        /// <typeparam name="T">Role type to add to the profile</typeparam>
        /// <returns>The currently built profile</returns>
        public Profile Add<T>(int amount = 1) where T : GameRole, new() {
            for (int i = 0; i < amount; i++)
                roles.Add(new T());
            return this;
        }
    }
}