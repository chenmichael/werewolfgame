﻿using System;
using System.Diagnostics;
using System.Threading.Tasks;
using WerewolfGame.Hubs.Interfaces;

namespace WerewolfGame.Hubs {
    /// <summary>
    /// Provides an instance of a werewolf game hub user.
    /// </summary>
    [DebuggerDisplay("{Username}")]
    public class User : IGameLobby {
        /// <summary>
        /// Construct a user with a given name and connection ID.
        /// These are required as every user always has a non-null name and ID.
        /// </summary>
        /// <param name="username">Initial username.</param>
        public User(Username username) {
            Username = username;
        }
        /// <summary>
        /// Returns the current lobby of the user or null if the user is not in a lobby.
        /// </summary>
        public Lobby? CurrentLobby { get; set; }
        /// <summary>
        /// Returns the current lobby of the user or throws an <see cref="ArgumentException"/> if the user is not in a lobby.
        /// </summary>
        /// <exception cref="ArgumentException">User is not in a lobby.</exception>
        public Lobby RequireLobby { get => CurrentLobby ?? throw new ArgumentException("You are not in a lobby!"); }
        /// <summary>
        /// Current username that the user is registered with.
        /// </summary>
        public Username Username { get; set; }

        /// <summary>
        /// Function must be called when the user disconnects such that the connection ID can be set.
        /// </summary>
        /// <returns></returns>
        public async Task OnDisconnectedAsync() {
            await LeaveLobby();
            ConnectionId = null;
        }

        /// <summary>
        /// Current connection ID that the user is connected with or null if the user is disconnected.
        /// </summary>
        public string? ConnectionId { get; set; }

        /// <summary>
        /// Current client connection that the user is connected with or null if the user is disconnected.
        /// </summary>
        public IGameClient? Client { get => ConnectionId is null ? null : Program.HubContext.Clients.Client(ConnectionId); }


        /// <inheritdoc cref="Lobby.LeaveLobby(User)"/>
        public async Task<Maybe> LeaveLobby() {
            if (CurrentLobby is not null)
                return await CurrentLobby.LeaveLobby(this);
            return Maybe.Successful;
        }

        #region Game Lobby Implementation
        /// <inheritdoc cref="IGameLobby.VoteForPlayer(string)"/>
        public async Task<Maybe> VoteForPlayer(string username) => await RequireLobby.VoteForPlayer(this, username);

        /// <inheritdoc cref="IGameLobby.StartGame()"/>
        public async Task<Maybe> StartGame() => await RequireLobby.StartGame(this);
        #endregion
    }
}
