﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using WerewolfGame.Hubs.Interfaces;
using WerewolfGame.Logic;

namespace WerewolfGame.Hubs {
    /// <summary>
    /// Provides a class for a werewolf game lobby.
    /// </summary>
    public class Lobby {
        /// <summary>
        /// Gets the unique identifier for the current lobby.
        /// </summary>
        public LobbyId ID { get; }
        /// <summary>
        /// Reference to the User that currently hosts this lobby.
        /// </summary>
        private User? Host;
        /// <summary>
        /// Raised when the lobby has no more members and shall be closed.
        /// Can be used for removing the lobby from active sessions.
        /// </summary>
        public event EventHandler? LobbyClosed;
        /// <summary>
        /// Gets the current status of the lobby.
        /// </summary>
        public LobbyStatus Status { get; private set; }
        /// <summary>
        /// Collection of members of this lobby.
        /// </summary>
        private readonly HashSet<User> members = new HashSet<User>();
        /// <summary>
        /// Object that handles the game logic once the lobby starts its game.
        /// </summary>
        private GameLogic? gameWorker;

        /// <summary>
        /// Instantiates an empty lobby with the given ID.
        /// </summary>
        /// <param name="id">Unique Lobby identifier.</param>
        public Lobby(LobbyId id) {
            ID = id;
            Status = LobbyStatus.Waiting;
        }

        /// <summary>
        /// Adds the given user to the lobby group.
        /// </summary>
        /// <param name="user">User to add to lobby.</param>
        private async Task AddToLobbyAsync(User user) {
            if (user.ConnectionId is not null)
                await Program.HubContext.Groups.AddToGroupAsync(user.ConnectionId, ID);
        }

        /// <summary>
        /// Removes the given user from the lobby group.
        /// </summary>
        /// <param name="user">User to remove from lobby.</param>
        private async Task RemoveFromLobbyAsync(User user) {
            if (user.ConnectionId is not null)
                await Program.HubContext.Groups.RemoveFromGroupAsync(user.ConnectionId, ID);
        }

        /// <summary>
        /// Join a lobby.
        /// </summary>
        /// <param name="user">User that joins the lobby.</param>
        /// <exception cref="ArgumentException">Lobby cannot be joined.</exception>
        public async Task<LobbyId> Join(User user) {
            if (Status != LobbyStatus.Waiting)
                throw new ArgumentException($"Cannot join {Status.ToString().ToLower()} lobby!");
            lock (members) {
                if (members.Count == 0) Host = user;
                members.Add(user);
            }
            user.CurrentLobby = this;
            await AddToLobbyAsync(user);
            return ID;
        }

        /// <summary>
        /// Leave a lobby.
        /// </summary>
        /// <param name="user">User that leaves the lobby.</param>
        public async Task<Maybe> LeaveLobby(User user) {
            // if user is host then new host must be selected.
            lock (members) {
                members.Remove(user);
                if (members.Count == 0) {
                    LobbyClosed?.Invoke(this, new EventArgs());
                    Status = LobbyStatus.Closed;
                } else
                    Host = members.First();
            }
            user.CurrentLobby = null;
            await RemoveFromLobbyAsync(user);
            return Maybe.Successful;
        }

        /// <inheritdoc cref="IGameLobby.VoteForPlayer(string)"/>
        public async Task<Maybe> VoteForPlayer(User user, string username) {
            var voting = gameWorker?.Voting;
            if (voting is null) return new ArgumentException("No voting is active!");
            return await voting.VoteForPlayer(user, username);
        }

        /// <summary>
        /// Starts the game.
        /// </summary>
        /// <param name="user">User that wants to start the game.</param>
        /// <exception cref="ArgumentException">Game already started.</exception>
        /// <exception cref="ArgumentException">You're not game host.</exception>
        /// <exception cref="ArgumentException">No valid profile for amount of players.</exception>
        public Task<Maybe> StartGame(User user) {
            if (user != Host) throw new ArgumentException("Only the game host can start the game!");
            if (Status != LobbyStatus.Waiting) throw new ArgumentException("Lobby is not waiting!");
            gameWorker = new GameLogic(members);
            gameWorker.StateChanged += StateChanged;
            return gameWorker.Start();
        }

        /// <summary>
        /// Set the lobby state when the game logic reports a status change.
        /// </summary>
        /// <param name="sender">Logic that sets the state</param>
        /// <param name="e">Current logic state</param>
        private void StateChanged(object? sender, LobbyStatus e) => Status = e;
    }
}