﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;

namespace WerewolfGame.Hubs {
    /// <summary>
    /// Provides a class for voting.
    /// Checks if users are allowed to vote and if their votes are legitimate.
    /// </summary>
    public class Voting {
        private readonly Dictionary<User, string?> voters;
        private readonly Dictionary<string, int> options;
        private readonly AutoResetEvent are = new AutoResetEvent(false);
        private readonly string message;
        private bool closed = false;
        private DateTime? voteEnd;
        /// <summary>
        /// Creates a new Voting where only the <paramref name="voters"/> are allowed to vote.
        /// </summary>
        /// <param name="message">Message to show to the voters.</param>
        /// <param name="voters">Collection of voters for this voting</param>
        /// <param name="options">Collection of options that are available</param>
        public Voting(string message, IEnumerable<User> voters, IEnumerable<string> options) {
            this.voters = voters.ToDictionary(i => i, i => (string?)null);
            if (this.voters.Count == 0) throw new ArgumentException("No voters allowed!");
            this.options = options.ToDictionary(i => i, i => 0);
            if (this.options.Count == 0) throw new ArgumentException("No voting options given!");
            this.message = message;
        }

        /// <summary>
        /// Collect votes from all voters.
        /// </summary>
        /// <param name="timeout">Timeout in seconds until vote is closed</param>
        /// <exception cref="ArgumentOutOfRangeException">The exception thrown when <paramref name="timeout"/> is negative.</exception>
        public async Task<IEnumerable<string>> CollectVotes(int timeout = 30) {
            await voters.Keys.GetClient().Awake(message, options.Keys, timeout);
            voteEnd = DateTime.Now.AddSeconds(timeout);
            var voteTimedout = false;
            var timeoutTask = Task.Run(async () => {
                await Task.Delay(timeout * 1000);
                voteTimedout = true;
                are.Set();
            });
            while (are.WaitOne() && !voteTimedout) {
                int missingVotes;
                lock (voters) missingVotes = voters.Count(i => i.Value is null);
                // If all votes were input break the timeout
                if (missingVotes == 0) break;
            }
            closed = true;
            await Task.WhenAll(voters.Select(i => {
                var client = i.Key.Client;
                return client?.Sleep(i.Value is not null ? $"You chose {i.Value}!" : "You didn't vote this round!", i.Value is not null) ?? Task.CompletedTask;
            }));
            // options that received max votes
            var maxVotes = options.Values.Max();
            return from option in options where option.Value == maxVotes select option.Key;
        }

        /// <inheritdoc cref="Interfaces.IGameLobby.VoteForPlayer(string)"/>
        public async Task<Maybe> VoteForPlayer(User user, string username) {
            if (closed) throw new ArgumentException("Voting is closed!");
            try {
                lock (voters) {
                    if (!voters.TryGetValue(user, out var voted)) throw new ArgumentException("User is not allowed to vote!");
                    if (voted is not null) throw new ArgumentException("User has already cast his vote!");
                    lock (options) {
                        if (!options.ContainsKey(username)) throw new ArgumentException($"'{username}' is not a valid voting option!");
                        options[username]++;
                    }
                    voters[user] = username;
                }
                are.Set();
                return Maybe.Successful;
            } catch (ArgumentException e) {
                var client = user.Client;
                if (client is not null)
                    await client.Awake(message, options.Keys, GetRemainingSeconds());
                return e;
            }
        }

        /// <summary>
        /// Gets the time that remains for this voting.
        /// </summary>
        /// <returns>The time in seconds left to vote.</returns>
        /// <exception cref="InvalidProgramException"><see cref="voteEnd"/> is null. Must be set to end time in <see cref="CollectVotes(int)"/>.</exception>
        private int GetRemainingSeconds()
            => (int)((voteEnd ?? throw new InvalidProgramException("Vote end is null!")) - DateTime.Now).TotalSeconds;
    }
}