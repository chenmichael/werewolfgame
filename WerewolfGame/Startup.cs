using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using WerewolfGame.Hubs;

namespace WerewolfGame {
    /// <summary>
    /// Provide the startup configuration class for the Werewolf game server.
    /// </summary>
    public class Startup {
        /// <summary>
        /// Instanciate a new startup class with the given <paramref name="configuration"/>.
        /// </summary>
        /// <param name="configuration">Startup configuration.</param>
        public Startup(IConfiguration configuration) {
            Configuration = configuration;
        }

        /// <summary>
        /// Configuration properties of the startup.
        /// </summary>
        public IConfiguration Configuration { get; }

        /// <summary>
        /// This method gets called by the runtime. Use this method to add services to the container.
        /// </summary>
        /// <param name="services">Services to configure</param>
        public void ConfigureServices(IServiceCollection services) {
            services.AddRazorPages();
            services.AddSignalR().AddNewtonsoftJsonProtocol();
        }

        /// <summary>
        /// This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        /// </summary>
        /// <param name="app">Application builder</param>
        /// <param name="env">Configuration environment</param>
        public void Configure(IApplicationBuilder app, IWebHostEnvironment env) {
            if (env.IsDevelopment()) {
                app.UseDeveloperExceptionPage();
            } else {
                app.UseExceptionHandler("/Error");
                // The default HSTS value is 30 days. You may want to change this for production scenarios, see https://aka.ms/aspnetcore-hsts.
                app.UseHsts();
            }

            app.UseHttpsRedirection();
            app.UseStaticFiles();

            app.UseRouting();

            app.UseAuthorization();

            app.UseEndpoints(endpoints => {
                endpoints.MapRazorPages();
                endpoints.MapHub<GameHub>(GameHub.DefaultPath);
            });
        }
    }
}
