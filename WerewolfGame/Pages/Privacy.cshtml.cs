﻿using Microsoft.AspNetCore.Mvc.RazorPages;
using Microsoft.Extensions.Logging;

namespace WerewolfGame.Pages {
#pragma warning disable CS1591 // Missing XML comment for publicly visible type or member
    public class PrivacyModel : PageModel {
#pragma warning disable IDE0052 // Remove unread private members
        private readonly ILogger<PrivacyModel> _logger;
#pragma warning restore IDE0052 // Remove unread private members

        public PrivacyModel(ILogger<PrivacyModel> logger) {
            _logger = logger;
        }

        public void OnGet() {
        }
    }
#pragma warning restore CS1591 // Missing XML comment for publicly visible type or member
}
