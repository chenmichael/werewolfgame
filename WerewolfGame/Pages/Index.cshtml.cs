﻿using Microsoft.AspNetCore.Mvc.RazorPages;
using Microsoft.Extensions.Logging;

namespace WerewolfGame.Pages {
#pragma warning disable CS1591 // Missing XML comment for publicly visible type or member
    public class IndexModel : PageModel {
#pragma warning disable IDE0052 // Remove unread private members
        private readonly ILogger<IndexModel> _logger;
#pragma warning restore IDE0052 // Remove unread private members

        public IndexModel(ILogger<IndexModel> logger) {
            _logger = logger;
        }

        public void OnGet() {

        }
    }
#pragma warning restore CS1591 // Missing XML comment for publicly visible type or member
}
